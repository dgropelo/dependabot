# frozen_string_literal: true

source "https://rubygems.org"

ruby "~> 2.7"

gem "anyway_config", "~> 2.3"
gem "bootsnap", ">= 1.4.2", require: false
gem "dependabot-omnibus", "~> 0.190.0"
gem "dry-validation", "~> 1.8"
gem "gitlab", "~> 4.18"
gem "graphql-client", "~> 0.18.0"
gem "lograge", "~> 0.12.0"
gem "mongoid", "~> 7.4"
gem "mongoid_rails_migrations", "~> 1.4"
gem "puma", "~> 5.6"
gem "rails", "~> 7.0.3"
gem "rails-healthcheck", "~> 1.4"
gem "rainbow", "~> 3.1"
gem "semantic_range", "~> 3.0"
gem "sentry-rails", "~> 5.3", require: false
gem "sentry-ruby", "~> 5.3", require: false
gem "sentry-sidekiq", "~> 5.3", require: false
gem "sidekiq", "~> 6.4.2"
gem "sidekiq_alive", "~> 2.1", require: false
gem "sidekiq-cron", "~> 1.4"
gem "terminal-table", "~> 3.0"
gem "yabeda-prometheus-mmap", "~> 0.3.0"
gem "yabeda-puma-plugin", "~> 0.6.0"
gem "yabeda-sidekiq", "~> 0.8.1"

group :test do
  gem "airborne", "~> 0.3.7"
  gem "allure-rspec", "~> 2.18.0"
  gem "capybara", "~> 3.37"
  gem "factory_bot_rails", "~> 6.2"
  gem "faker", "~> 2.21"
  gem "httparty", "~> 0.20.0"
  gem "pry-byebug", "~> 3.9"
  gem "reek", "~> 6.1", require: false
  gem "rspec", "~> 3.11"
  gem "rspec_junit_formatter", "~> 0.5.1"
  gem "rspec-rails", "~> 5.1.2"
  gem "rspec-sidekiq", "~> 3.1", require: false
  gem "rubocop", "~> 1.30.0", require: false
  gem "rubocop-performance", "~> 1.14.0", require: false
  gem "rubocop-rails", "~> 2.14", require: false
  gem "rubocop-rspec", "~> 2.11", require: false
  gem "simplecov", "~> 0.21.2", require: false
  gem "simplecov-cobertura", "~> 2.1.0", require: false
  gem "simplecov-console", "~> 0.9.1", require: false
end

group :development do
  gem "git", "~> 1.11", require: false
  gem "pry-rails", "~> 0.3.9"
  gem "semver2", "~> 3.4", require: false
  gem "solargraph", "~> 0.45.0", require: false
  gem "spring", "~> 4.0.0", require: false
  gem "spring-commands-rspec", "~> 1.0.4", require: false
end
